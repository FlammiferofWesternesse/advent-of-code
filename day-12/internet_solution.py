"""
I did not write the functions defined below. I copied them to study them. They 
originally appeared on the Advent of Code Subreddit, posted by u/NikitaSkybytskyi.
"""

import itertools

def match(records: str, nums: list[int]) -> bool:
    return nums == [
        sum(1 for _ in grouper)
        for key, grouper in itertools.groupby(records)
        if key == "#"
    ]

def brute_force(records: str, nums: list[int]) -> int:
    gen = ("#." if letter == "?" else letter for letter in records)
    return sum(match(candidate, nums) for candidate in itertools.product(*gen))



for c_line in [line.removesuffix('\n') for line in open("day-12/hot-spring-diagram-sample-1.txt", 'r').readlines()]:
    line_records, line_nums = c_line.split(' ')
    if line_records == "":
        continue
    line_nums = [int(num) for num in line_nums.split(',')]
    # print(line_nums)
    print(brute_force(line_records, line_nums))
# print(brute_force("???.###", [1, 1, 3]))
