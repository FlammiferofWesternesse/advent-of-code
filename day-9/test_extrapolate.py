from unittest import TestCase
import pytest
from extrapolate import ExtrapolateOASIS
from extrapolate_rev import ReverseExtrapolate

class TestExtrapolateOASIS(TestCase, ExtrapolateOASIS):
    def test_solve_oasis_sample_individual(self):
        # Arrange
        oasis_histories = [
            [0, 3, 6, 9, 12, 15], 
            [1, 3, 6, 10, 15, 21], 
            [10, 13, 16, 21, 30, 45]
        ]

        # Act
        results = [] 
        for history in oasis_histories:
            self.oasis_histories = [history]
            result = self.extrapolate_oasis()
            results.append(result)

        # Assert
        self.assertEqual(results[0], 18)
        self.assertEqual(results[1], 28)
        self.assertEqual(results[2], 68)


    def test_solve_oasis_sample_together(self):
        # Arrange
        self.oasis_histories = [
            [0, 3, 6, 9, 12, 15], 
            [1, 3, 6, 10, 15, 21], 
            [10, 13, 16, 21, 30, 45]
        ]

        # Act
        result = self.extrapolate_oasis()

        # Assert
        self.assertEqual(result, 114)

class TestReverseExtrapolate(TestCase, ReverseExtrapolate):
    def test_solve_oasis_sample_individual(self):
        # Arrange
        oasis_histories = [
            [0, 3, 6, 9, 12, 15], 
            [1, 3, 6, 10, 15, 21], 
            [10, 13, 16, 21, 30, 45]
        ]

        # Act
        results = [] 
        for history in oasis_histories:
            self.oasis_histories = [history]
            result = self.extrapolate_oasis()
            results.append(result)

        # Assert
        self.assertEqual(results[0], -3)
        self.assertEqual(results[1], 0)
        self.assertEqual(results[2], 5)


    def test_solve_oasis_sample_together(self):
        # Arrange
        self.oasis_histories = [
            [0, 3, 6, 9, 12, 15], 
            [1, 3, 6, 10, 15, 21], 
            [10, 13, 16, 21, 30, 45]
        ]

        # Act
        result = self.extrapolate_oasis()

        # Assert
        self.assertEqual(result, 2)
