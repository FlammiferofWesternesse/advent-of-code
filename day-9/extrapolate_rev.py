from extrapolate import ExtrapolateOASIS

class ReverseExtrapolate(ExtrapolateOASIS):

    # All changes occur on lines 17 and 19:
    # On both lines history[-1] -> history[0] and + -> -
    def breakdown(self, history: list[int]) -> int:
            differences = []
            for item_ind in range(len(history)):
                item = history[item_ind]
                if item_ind == 0:
                    prev_item = item
                    continue
                differences.append(item - prev_item)
                prev_item = item
            if len(set(differences)) == 1:    
                return history[0] - differences[0]
            else:
                return history[0] - self.breakdown(differences)
        
if __name__ == "__main__":
    baxtraplationist = ReverseExtrapolate('day-9/OASIS.txt')

    print(baxtraplationist.extrapolate_oasis())
