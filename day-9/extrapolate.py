"""
Overall strategy: write a method to be called recursively on each line to find the
extrapolated value, reducing the results to a grand total.
"""

class ExtrapolateOASIS:
    def __init__(self, file_name: str) -> None:
        with open(file_name) as file:
            self.oasis_data = [line.removesuffix('\n') for line in file.readlines()]
        self.oasis_histories = self.parse_oasis()

    # This is the recursive method, taking a list of ints for its arg, and returns an
    # int representing the extropolation of the line's nest number
    def breakdown(self, history: list[int]) -> int:
        differences = []
        for item_ind in range(len(history)):
            item = history[item_ind]
            if item_ind == 0:
                prev_item = item
                continue
            differences.append(item - prev_item)
            prev_item = item
        if len(set(differences)) == 1:    
            return history[-1] + differences[0]
        else:
            return history[-1] + self.breakdown(differences)


    # This method groups the extrpolated value from each line and adds it to a total,
    # then it returns that total.
    def extrapolate_oasis(self) -> int:
        extrapolation_total = 0
        for history_ind in range(len(self.oasis_histories)):
            extrapolation_total += self.breakdown(self.oasis_histories[history_ind])
        return extrapolation_total
        
    # Read the document and convert its contents into a matrix of integers to store in state.
    def parse_oasis(self) -> list[list[int]]:
        oasis_list = [[int(num) for num in line.split(' ')] for line in self.oasis_data]
        return oasis_list



if __name__ == "__main__":
    extraplationist = ExtrapolateOASIS('day-9/OASIS.txt')

    print(extraplationist.extrapolate_oasis())
