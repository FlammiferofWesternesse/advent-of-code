# Part One Summary

### Method

The overall strategy here is to use a recursive method to find the sequential differences in each list of numbers and call itself as long as the differences are not all the same value.

1. `__init(self, file_name) -> None`
2. `breakdown(self, history: list[int]) -> int`
3. `extrapolate_oasis(self) -> int`
4. `parse_oasis(self) -> list[list[int]]`

`__init__` calls `parse_oasis` to get `oasis_histories`.

`breakdown` is the recursive method which takes a list of numbers, looks at the sequential differences, and either returns the final item of the list plus the sole value in `set(differences)` or the final item of the list plus a recursive call to `breakdown(differences)`.

`extrapolate` calls `breakdown` for each line of `oasis_histories` to get a total value of all extrapolations.

`parse_oasis` reads `self.oasis_data` and returns a matrix of integers.

# Part Two Summary

### Method

This may be the simplest part two of them all.
On lines 17 and 19, brakdown is modified so that it is returning the first number in the list minus the difference. The exact reverse of before.
