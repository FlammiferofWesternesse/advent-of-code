# This function takes a line from the source doc, cleans it, and returns a dictionary
# whose values are the game number, a dictionary of lists, a boolean for whether the
# game represented is possible, and the minumum set of cubes required for it to be
# possible.
def parse(line: str):

    # create a baseline low set
    minimum_set = {
        "red": 0,
        "green": 0,
        "blue": 0
    }
    game_number = ""

    # Parse out the game number
    for i in range(5, 8):
        if line[i].isdigit():
            game_number += line[i]
        else:
            break

    # Clean the string
    line = line.removeprefix(f"Game {game_number}:")
    line = line.removesuffix('\n')

    # Create a matrix where each nested list represents a handful
    handfuls = [item.split(',') for item in line.split(";")]

    # Create a handfuls_dict with {str: int} so that the handfuls
    # represented in the matrix above can be easily compared to the min_set
    for handful in handfuls:
        handfuls_dict = {}
        for item in handful:
            num = item[:3]
            color = item[3:]
            handfuls_dict[color.strip()] = int(num.strip())

        # compare the values in this handfuls_dict to the minimum set and raise the
        # minimum required number if any new values are higher
        for color in handfuls_dict:
            if minimum_set[color] < handfuls_dict[color]:
                minimum_set[color] = handfuls_dict[color]

    # Put the minimum_set in the return dict
    game_object = {
        "minimum_set": minimum_set
    }
    return game_object

# Much simpler than part one: THis function takes the lines, puts them through parse(),
# and reduces the result to return an integer representing the total of each game's
# minimum dice requirements multiplied together.
def powers_of_minimum_sets(game_data: list[str]):

    # Take every line in game_data and parse it into a game_object
    games = []
    for line in game_data:
        games.append(parse(line))

    # Use a nested reduction to sum up the products of minimum_set values
    total = 0
    for game in games:
        sub_total = 1
        for minimum in game["minimum_set"].values():
            sub_total *= minimum
        total += sub_total
    return total

with open("day-2/cube-game-data.txt", 'r') as games:
    print(powers_of_minimum_sets(games.readlines()))
