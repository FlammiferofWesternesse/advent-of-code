# Part One Summary

### Method

For this problem I used to functions in somewhat similar manner to Day 1: Trebuchet; I had a parser, and I had a calculator.

The parser does a lot in this one:
Ther parser takes a line of the source doc as its arg, and returns the following dict:

```python
game_object = {
    "game_number": game_number, # int
    "handfuls": handfuls_dicts, # list[dict]
    "is_possible": True         # bool
}
```

Each `dict` in the `handfuls_dicts` list has the color: number pairs for each color in each handful per game.

The `is_possible_count()` function takes the list of lines returned by `.readlines()` on line 81 and, calls the `parse()` function, and then checks the  `handfuls_dicts` in each `game_object` to determine is that game is possible with the current config. If it is not, the `is_possible` value for that object is set to `False` and the nested loops break to continue iterating `games`.

After Each `game_object` has been checked, the number of possible games is counted by iterative reduction, and the total is returned.

**NOTE:** As I write this, I realize that I could make this function a little more efficient by just using a variable to keep a running count of games that are proven to be impossible, and then return `len(games) - impossible_game_count`.

# Part Two Summary

### Method

This problem is pretty radically different.
Because the new problem doesn't actually care about a total of the numbers of dice (I'm sorry, "cubes"), the only thing that `parse()` needs to return in the minimum set. `game_number` is only used to clean the string, `handfuls_dict` is still used to compared against `minimum_set`, and `handfuls_dicts` is done away with entirely, as is `is_possible`.

Instead of `is_possible_count()` we now have `powers_of_minimum_sets()`. This function has the same params, and it still uses parse to create a list of `game_objects`, but it skips all the comparison logic from before and does a nested reduction to get the sum of the products of each game's minimum set. It returns the total of that reduction.
