# This function takes a line from the source doc, cleans it, and returns a dictionary
# whose values are the game number, a dictionary of lists, and a boolean for whether
# the game represented is possible.
def parse(line: str) -> dict:
    game_number = ""

    # Parse out the game number
    for i in range(5, 8):
        if line[i].isdigit():
            game_number += line[i]
        else:
            break

    # Clean the string and convert game_number to int
    line = line.removeprefix(f"Game {game_number}:")
    line = line.removesuffix('\n')
    game_number = int(game_number)

    # Create a matrix where each nested list represents a handful
    handfuls = [item.split(',') for item in line.split(";")]

    # Populate handfuls_dicts with {str: int} dicts so that the handfuls
    # represented in the matrix above will now be mapped.
    handfuls_dicts = []
    for handful in handfuls:
        handfuls_dict = {}
        for item in handful:
            num = item[:3]
            color = item[3:]
            handfuls_dict[color.strip()] = int(num.strip())
        handfuls_dicts.append(handfuls_dict)

    # Bundle the game_number, handfuls_dicts, and is_possible=True in the return dict
    game_object = {
        "game_number": game_number,
        "handfuls": handfuls_dicts,
        "is_possible": True
    }
    return game_object

# This function takes the complete list of lines from the source document, calls the
# parser, and then returns the total number of game_objects whose "is_possible" value
# is True
def is_possible_count(game_data: list[str]):
    # The stipulated cube limits for this problem
    config = {
        "red": 12,
        "green": 13,
        "blue": 14,
    }

    # Take every line in game_data and parse it into a game_object
    games = []
    for line in game_data:
        games.append(parse(line))

    # Looking at each game_object...
    for game in games:
        # ... looking at each individual handful_dict in the handfuls_dicts list...
        for handful in game["handfuls"]:
            # ... and comparing them with each color in config, if one handful has a
            # color key with a value greater than its equivalent in config, is_possible
            # to False and break out of the config and game["handfuls"] loops.
            for color in config:
                if not handful.get(color):
                    continue
                if handful[color] > config[color]:
                    game["is_possible"] = False
                    break
            if not game["is_possible"]:
                break

    # Use a simple reduction to count how many games are possible and return the result
    total = 0
    for game in games:
        if game["is_possible"]:
            total += game["game_number"]
    return total

with open("day-2/cube-game-data.txt", 'r') as games:
    print(is_possible_count(games.readlines()))
