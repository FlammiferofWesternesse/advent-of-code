# Introduction

Each directory in this repository is designed with the purpose of solving the challenges posted on [Advent of Code](https://adventofcode.com/2023/).
Each directory has the inputs and programs to solve each day's problem. Also contained within each directory is an instructions file with the original text, and a README.md file explaining explaining the solution in brief.

Part of the purpose of this process is practicing better documenting, notetaking, planning, and class-oriented design, even when the employment of COP or OOP concepts is unnecessary at this scale.

# The Problems:

Each day's problems are as follows:
- Day 1: Trebuchet
- Day 2: Cube Game
- Day 3: Engine Schematics
- Day 4: Scratch Cards
- Day 5: Almanac

### Requirements:
- Python 3.10 or later
- pytest
