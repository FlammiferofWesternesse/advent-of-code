from io import TextIOWrapper
import inspect


# Takes a dict or a list, an int, and an optional int. It modifies the given data
# structure with the given arguments and returns the modified data structure
def data_add(data_struct: dict|list, char_ind, num=None):
    if type(data_struct) == dict:
        if not data_struct.get(char_ind):
            data_struct[char_ind] = []
        data_struct[char_ind] = num
    else:
        data_struct.append(char_ind)
    return data_struct

# Takes the parts list and append the digital parts string to it, and returns the list
def add_to_parts(parts: list, part_str: str):
    part = int(part_str)
    parts.append(part)
    return parts

# Takes a line
def parse_line(line: str):
    number_dict = {}
    symbol_list = []
    line = line.removesuffix('\n')
    cur_num = ""
    for char_ind in range(len(line)):
        cur_char = line[char_ind]
        if (
            not cur_char.isalnum() and
            cur_char != "."
        ):
            symbol_list = data_add(symbol_list, char_ind)
        elif cur_char.isdigit():
            cur_num += cur_char
            if char_ind == len(line) - 1:
                number_dict = data_add(number_dict, char_ind, cur_num)
                cur_num = ""
            elif not line[char_ind + 1].isdigit():
                number_dict = data_add(number_dict, char_ind, cur_num)
                cur_num = ""
    return {"numbers": number_dict, "symbols": symbol_list}


def parse_schematic(schema: TextIOWrapper):
    parts_list = []
    schema_lines = schema.readlines()
    prev_line_data_boxes = {"numbers":{}, "symbols":[]}
    for line_ind in range(len(schema_lines)):
        line = schema_lines[line_ind]
        line_data_boxes = parse_line(line)
        symbol_inds = line_data_boxes["symbols"]
        numbers = line_data_boxes["numbers"]
        prev_symbol_inds = prev_line_data_boxes["symbols"]
        prev_numbers = prev_line_data_boxes["numbers"]
        used_numbers = []
        for symbol_ind in symbol_inds:
            for number in numbers:
                if (
                    symbol_ind + 1 == (number - len(numbers[number]) + 1) or
                    symbol_ind - 1 == number
                ):
                    parts_list = add_to_parts(parts_list, numbers[number])
                    used_numbers.append(number)
            for number in used_numbers:
                del numbers[number]
            used_numbers = []
            for number in prev_numbers:
                for ind in range(number - len(prev_numbers[number]) + 1, number + 1):
                    if (
                        (
                            ind == symbol_ind - 1 or
                            ind == symbol_ind or
                            ind == symbol_ind + 1
                        )
                    ):
                        parts_list = add_to_parts(parts_list, prev_numbers[number])
                        used_numbers.append(number)
                        break
            for number in used_numbers:
                del prev_numbers[number]
            used_numbers = []
        for number in numbers:
            for ind in range(number - len(numbers[number]), number + 2):
                if ind in prev_symbol_inds:
                    parts_list = add_to_parts(parts_list, numbers[number])
                    used_numbers.append(number)
                    break
        for number in used_numbers:
            del numbers[number]
        used_numbers = []
        line_data_boxes["numbers"] = numbers
        prev_line_data_boxes = line_data_boxes
    return parts_list

with open("day-3/engine-schematic.txt", 'r') as schematic:
    parts_list = parse_schematic(schematic)

print(parts_list)
print(len(parts_list), sum(parts_list))
