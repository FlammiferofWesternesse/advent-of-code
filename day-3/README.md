# Part One Summary

### Method

This one's a mess.

My general approach is to take and save each part number and index, and each symbol index in each line, and compare the appropriate indices or ranges or indices for the current line and previous line.

This one uses four functions:
1. `data_add()`
2. `add_to_parts()`
3. `parse_line()`
4. `parse_schematic()`

`data_add()` takes a list of indices for symbols or a dictionary of parts indices and numbers, an index number, and an optional part number, and adds the latter two to the appropriate data structure (Numbers are identified by the index of the final digit). It returns the modified data structure.  

`add_to_parts()` takes a list of part numbers, a part number, converts it to int, and adds the number to the list. It returns the modified list.  

`parse_line()` takes a line from the list returned by `readlines()` on line 48. It cleans the string and iterates through its characters, using `data_add()` to store the relevant indices. It return a dictionary with a dictionary named "numbers, and a list named "symbol_list."  

`parse_schematic()` takes a TextIOWrapper object, reads it, and iterates through the lines. It calls parse_line(). It then takes all the stored data and iterated through the symbols list first. It compares the symbol indices to the current lines number indicies first, then against those of the previous line. It then iterates through current line numbers' indices against the previous line's symbol indices. Matches are added to the list 

`parts_list` In clean-up, it updates the numbers dictionary with all used numbers removed, and saves the current line's data structures into the `previous_line_data_boxes` dictionary. It returns the parts list

# Part Two Summary

### Method

**This method is still incomplete**

For this solution, I tried to refactor from Function Oriented Programming to Class Oriented Programming.
