from io import TextIOWrapper
import inspect

class EngineSchematicCalc:
    def __init__(self, schematic_name: str) -> None:
        schematic = open(schematic_name, 'r')
        self.schematic_lines = schematic.readlines()
        self.line_data_boxes = {"numbers": {}, "symbols": []}
        self.prev_line_data_boxes = {"numbers": {}, "symbols": []}
        self.gears = {}
        self.parts_list = []
        self.line = ""

    def data_add(self, data_struct: dict|list, char_ind, num=None):
        if type(data_struct) == dict:
            if not data_struct.get(char_ind):
                data_struct[char_ind] = []
            data_struct[char_ind] = num
        else:
            data_struct.append(char_ind)
        return data_struct

    def add_gear(self, line_ind, char_ind):
        gear_name = str(line_ind) + ':' + str(char_ind)
        if self.gears.get(gear_name) is None:
            self.gears[gear_name] = []

    def add_gear_number(self, gear_number, number):
        self.gears[gear_number].append(number)

    def remove_gear(self, gear_name):
        del self.gears[gear_name]

    def get_gears(self):
        return self.gears

    def add_to_parts_list(self, part_str: str):
        part = int(part_str)
        self.parts_list.append(part)

    def set_line_boxes(self, num_dict=None, symbol_list=None):
        if num_dict is not None:
            self.line_data_boxes["numbers"] = num_dict
        if symbol_list is not None:
            self.line_data_boxes["symbols"] = symbol_list

    def set_prev_line_boxes(self):
        self.prev_line_data_boxes = self.line_data_boxes

    def parse_line(self, line_ind):
        number_dict = {}
        symbol_list = []
        _cur_num = ""
        self.line = self.line.removesuffix('\n')
        for char_ind in range(len(self.line)):
            _cur_char = self.line[char_ind]
            if (
                not _cur_char.isalnum() and
                _cur_char != "."
            ):
                symbol_list = self.data_add(symbol_list, char_ind)
                if _cur_char == "*":
                    self.add_gear(line_ind, char_ind)
            elif _cur_char.isdigit():
                _cur_num += _cur_char
                if char_ind == len(self.line) - 1:
                    number_dict = self.data_add(number_dict, char_ind, _cur_num)
                    _cur_num = ""
                elif not self.line[char_ind + 1].isdigit():
                    number_dict = self.data_add(number_dict, char_ind, _cur_num)
                    _cur_num = ""
        self.set_line_boxes(number_dict, symbol_list)

    # This method is the backbone of the parsing process. It begins by iterating through
    # and "selecting" lines from the schematic_lines list.
    def parse_schematic(self):
        for line_ind in range(len(self.schematic_lines)):
            _prev_symbol_inds = self.prev_line_data_boxes["symbols"]
            _prev_number_inds = self.prev_line_data_boxes["numbers"]

            # This part of the for loop selects and parses the line.
            self.line = self.schematic_lines[line_ind]

            self.parse_line(line_ind)

            # Takes the datasets from parsing and creates variable to use
            symbol_inds = self.line_data_boxes["symbols"]
            number_inds = self.line_data_boxes["numbers"]
            _used_number_inds = []


            for symbol_ind in symbol_inds:
                for number_ind in number_inds:
                    if (
                        symbol_ind + 1 == (
                            number_ind - len(number_inds[number_ind]) + 1
                        ) or
                        symbol_ind - 1 == number_ind
                    ):

                        self.add_to_parts_list(number_inds[number_ind])
                        _used_number_inds.append(number_ind)
                for number_ind in _used_number_inds:
                    del number_inds[number_ind]
                _used_number_inds = []
                for number_ind in _prev_number_inds:
                    for ind in range(
                        number_ind - len(_prev_number_inds[number_ind]) + 1, number_ind + 1
                    ):
                        if (
                            (
                                ind == symbol_ind - 1 or
                                ind == symbol_ind or
                                ind == symbol_ind + 1
                            )
                        ):
                            self.add_to_parts_list(_prev_number_inds[number_ind])
                            _used_number_inds.append(number_ind)
                            break
                for number_ind in _used_number_inds:
                    del _prev_number_inds[number_ind]
                _used_number_inds = []
            for number_ind in number_inds:
                for ind in range(
                    number_ind - len(number_inds[number_ind]), number_ind + 2
                ):
                    if ind in _prev_symbol_inds:
                        self.add_to_parts_list(number_inds[number_ind])
                        _used_number_inds.append(number_ind)
                        break
            for number_ind in _used_number_inds:
                del number_inds[number_ind]
            _used_number_inds = []
            for gear_name in self.gears:
                gear_name_div = str(gear_name).find(':')
                gear_ind = int(gear_name[gear_name_div + 1:])
                for number_ind in _prev_number_inds:
                    for ind in range(
                        number_ind - len(_prev_number_inds[number_ind]) + 1, number_ind + 1
                    ):
                        if (
                            (
                                ind == gear_ind - 1 or
                                ind == gear_ind or
                                ind == gear_ind + 1
                            )
                        ):
                            self.add_gear_number(gear_name, _prev_number_inds[number_ind])
                for number_ind in number_inds:
                    if (
                        gear_ind + 1 == (
                            number_ind - len(number_inds[number_ind]) + 1
                        ) or
                        gear_ind - 1 == number_ind
                    ):
                        self.add_gear_number(gear_name, number_inds[number_ind])
                if len(self.gears[gear_name]) > 2:
                    self.remove_gear(gear_name)
            self.set_line_boxes(num_dict=number_inds)
            self.set_prev_line_boxes()

    def get_parts(self):
        return self.parts_list

schematic_suite = EngineSchematicCalc("engine-schematic-test2.txt")

schematic_suite.parse_schematic()

parts_list = schematic_suite.get_parts()

print(schematic_suite.get_gears())
print(parts_list)
print(sum(parts_list), len(parts_list))
