class BeamTracer:
    def __init__(self, file_name: str) -> None:
        with open(file_name, 'r') as file:
            self.beam_data = [line.removesuffix('\n') for line in file.readlines()]
        self.beams = [((0, 0), 1)] # Starting Tile and direction
        self.beams_history = {((0, 0), 1): True}
        self.directions = [(1, -1), (0, 1), (1, 1), (0, -1)]
        self.grid_w = len(self.beam_data[0])
        self.grid_h = len(self.beam_data)

    def tracer(self):
        energized_tiles = {}
        iter = 0
        while self.beams:
            cur_beam_pos, cur_beam_dir = self.beams.pop()
            current_tile = self.beam_data[cur_beam_pos[1]][cur_beam_pos[0]]
            if energized_tiles.get(cur_beam_pos) is None:
                energized_tiles[cur_beam_pos] = True
            # The following 4 options are the circumstances under which beams change 
            # direction or split directions.
            if current_tile == '|' and cur_beam_dir in [1, 3]:
                cur_beam_dir = 0
                if self.beams_history.get((cur_beam_pos, 2)) is None:
                    self.beams.append((cur_beam_pos, 2))
                    self.beams_history[cur_beam_pos, 2] = True
                else: 
                    continue
            elif current_tile == '-' and cur_beam_dir in [0, 2]:
                cur_beam_dir = 1
                if self.beams_history.get((cur_beam_pos, 3)) is None:
                    self.beams.append((cur_beam_pos, 3))
                    self.beams_history[(cur_beam_pos, 3)] = True
                else:
                    continue
            elif current_tile == '\\':
                if self.directions[cur_beam_dir][0] == 0:
                    cur_beam_dir = 2 if cur_beam_dir == 1 else 0
                else:
                    cur_beam_dir = 3 if cur_beam_dir == 0 else 1
            elif current_tile == '/':
                if self.directions[cur_beam_dir][0] == 0:
                    cur_beam_dir = 0 if cur_beam_dir == 1 else 2
                else:
                    cur_beam_dir = 1 if cur_beam_dir == 0 else 3
            if self.directions[cur_beam_dir][0] == 0: # East-West, change current_beam[0][0]
                if (
                    cur_beam_pos[0] + self.directions[cur_beam_dir][1] == self.grid_w or
                    cur_beam_pos[0] + self.directions[cur_beam_dir][1] < 0
                ):
                    continue
                else:
                    new_beam_pos = (
                        cur_beam_pos[0] + self.directions[cur_beam_dir][1], 
                        cur_beam_pos[1]
                    )
                    if self.beams_history.get((new_beam_pos, cur_beam_dir)) is None:
                        self.beams.append((new_beam_pos, cur_beam_dir))
                        self.beams_history[(new_beam_pos, cur_beam_dir)] = True
                    else:
                        continue
            else: # North-South, change current_beam[0][1]
                if (
                    (cur_beam_pos[1] + self.directions[cur_beam_dir][1]) == self.grid_h or
                    cur_beam_pos[1] + self.directions[cur_beam_dir][1] < 0
                ):
                    continue
                else:
                    new_beam_pos = (
                        cur_beam_pos[0], 
                        cur_beam_pos[1] + self.directions[cur_beam_dir][1]
                    )
                    if self.beams_history.get((new_beam_pos, cur_beam_dir)) is None:
                        self.beams.append((new_beam_pos, cur_beam_dir))
                        self.beams_history[(new_beam_pos, cur_beam_dir)] = True
                    else:
                        continue
            iter += 1
        print(len(energized_tiles))
    


if __name__ == "__main__":
    beamer = BeamTracer("day-16/beam-path.txt")

    beamer.tracer()
