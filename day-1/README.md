# Part One Summary

### Method

I used two function to accomplish this first task: 
1. `clean()` 
2. `get_vals()`

First, `get_vals()` is called on line 25, with `inputs.readlines()` as its calibration argument. The function iterates through each line in the list and searches with a regular expression for two digits, and if that returns `None`, then it searches for one.

`get_vals()` then calls `clean()`, passing the Match object returned by the `.search()` method and the current line string. `clean()` uses the `.start()` and `.end()` methods to find the desired digits and returns them concatenated and converted to an `int`.

Through a simple reductive process, `get_vals()` sums up each integer returned this way to get the final total.

# Part Two Summary

### Method

Because this problem is still seeking the same output, that is, the sum of all parsed numbers, the task of upgrading my algorithm only required me to add one line to `get_vals()` and one new function: `normalize()`.

`normalize()` takes the line strings from the `.readlines()` on line 44 and compares the string against each number in a dictionary to find the number words. If the number word occurs, then `.replace()` is called, removing the number word string with a string starting with a 3 character string which includes the word's  first letter, its digit equivalent, and its last letter, in that order. This is done so that if the number is sharing a first or last letter with another number, e.g. `"eightwo"`, then it will result in a normalized string that looks like this: `"e8t2o"`.

This normalized string is then put through the same process as in part one. The left behind letters have no effect on the the regex reads the string.
