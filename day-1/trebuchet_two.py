import re

# This function takes the Match object from line 16 or 18 and the
# line string and uses the extracts the digits from [0] and [-1]
# of the substring described by re.Match. Returns an integer of the
# two *concatenated* digits.
def clean(match: re.Match, current_line: str):
    first = match.start()
    second = match.end() - 1
    return int(current_line[first] + current_line[second])


# This function sets up a dictionary mapping number digits to number words.
# It iterates through the keys, checking if the values occur at all in each
# doc line. If so, the word substring is replaced like so:
# "eight" -> "e8t".
# This is done to preserve shared letters, e.g. "oneightwo" -> "o1e8t2o"
def normalize(current_line:str):
    numbers = {
        "0": "zero",
        "1": "one",
        "2": "two",
        "3": "three",
        "4": "four",
        "5": "five",
        "6": "six",
        "7": "seven",
        "8": "eight",
        "9": "nine",
    }
    for number in numbers:
        if numbers[number] in current_line:
            current_line = current_line.replace(numbers[number], f"{numbers[number][0]}{number}{numbers[number][-1]}")
    return current_line

# Takes the lines of the .txt doc as returned by .readlines() and
# iterates through them to find the substrings that are bounded by
# digit characters
def get_vals(calibration_data):
    total_vals = 0

    # matches a substr of two digits having any # chars in between
    find_nums = re.compile(r"\d.*\d")
    # matches the first occurance of a digit
    find_num = re.compile(r"\d")

    for index in range(len(calibration_data)):
        nums = 0
        line = calibration_data[index]
        # Modify line str for part 2
        line = normalize(line)

        # search for first AND last digits:
        nums = find_nums.search(line)
        if nums is None: # ^ returns None if only one digit in str
            nums = find_num.search(line) # searches for one digit
        val = clean(nums, line)
        total_vals += val

    return total_vals


with open('day-1/trebuchet-data.txt', 'r') as data:
    print(get_vals(data.readlines()))
