from boat_race import BoatRaceCalc

class ImprovedBoatRaceCalc(BoatRaceCalc):
    def parse_data(self) -> tuple:
        # We only need the one tuple, so we'll change the return here
        return super().parse_data()[0]
    
    # This is just a single iteration of the same process we used for each race before
    def race_calc(self):
        ways_to_win = 0
        time = self.races[0]
        half_time = time // 2
        while half_time * (time - half_time) > self.races[1]:
            ways_to_win += 1
            half_time -= 1
        if time % 2 == 0:
            ways_to_win = (ways_to_win - 1) * 2 + 1
        else:
            ways_to_win *= 2
        return ways_to_win

if __name__ == "__main__":
    racerizer = ImprovedBoatRaceCalc('day-6/races-fixed.txt')

    print(racerizer.race_calc())
