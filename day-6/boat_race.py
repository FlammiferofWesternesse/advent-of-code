"""
Overall Strategy:
The following class very simply cleans up the two lines of the input document and turns
the values into a list of tuples in the form (time, distance).
The method for solving each number of ways to win is simple: the most efficient way to 
win is (x/2) * (x/2) for even numbers, or ((x - 1) / 2) * ((x + 1) / 2) (and vice-versa) 
in the case of odd numbers. I begin by taking the halfway point and I iterate downward 
until I hit a factoring that no longer reaches the distance.
If the time figure is even, we double ways_to_win - 1 and add 1 back into the dividend 
(because reciprocal ways are valid). If it is odd, then we can simply double it.
"""

class BoatRaceCalc:
    def __init__(self, races: str) -> None:
        with open(races, 'r') as race_file:
            self.race_data = race_file.readlines()
        self.races = self.parse_data()
    
    # Simply cleans, and listifies lines from the file
    def clean_string_and_listify(self, line):
        return [
            int(number) for number in line[10:].removesuffix("\n").split(' ')
        ]

    # This method is only called by __init__(). It calls the cleaning function and 
    #  returns a list of tuple (time, distance) to store in state
    def parse_data(self) -> list:
        times = self.clean_string_and_listify(self.race_data[0])
        distances = self.clean_string_and_listify(self.race_data[1])
        return [tup for tup in zip(times, distances)]
        
    # This function iterates through each tuple in races, uses the mathematic method
    # outlined in "overall strategy", and makes a list of each race's count of ways 
    # to win, and performs a reduction to return the total of those values' products 
    def race_calc(self):
        total_margin = 1
        ways_list = []
        for race_ind in range(len(self.races)):
            ways_to_win = 0
            time = self.races[race_ind][0]
            half_time = time // 2
            while half_time * (time - half_time) > self.races[race_ind][1]:
                ways_to_win += 1
                half_time -= 1
            if time % 2 == 0:
                ways_to_win = (ways_to_win - 1) * 2 + 1
            else:
                ways_to_win *= 2
            ways_list.append(ways_to_win)
            ways_to_win = 0
        for way in ways_list:
            total_margin *= way
        return total_margin

if __name__ == "__main__":
    racerizer = BoatRaceCalc('day-6/races.txt')

    print(racerizer.race_calc())
    
