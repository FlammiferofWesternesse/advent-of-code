"""
Overall Strategy:
It seems that it will be easiest to write a function that "tilts" the axis of the matrix 
for that "north" becomes the right side instead of the top side.
"""
class RockRoller:
    def __init__(self, file_name: str) -> None:
        with open(file_name, 'r') as rock_file:
            self.rock_data = [line.removesuffix('\n') for line in rock_file.readlines()]

    def bubble_sort(self, char_list: list[str]) -> list[str]:
        unsorted = True
        while unsorted:
            unsorted = False
            for char_ind in range(len(char_list) - 1):
                if char_list[char_ind] == 'O' and char_list[char_ind + 1] == '.':
                    char_list[char_ind], char_list[char_ind + 1] = char_list[char_ind + 1], char_list[char_ind]
                    unsorted = True
        return char_list

    def calc_load(self, rock_positions: dict) -> int:
        total = 0
        for key, value in rock_positions.items():
            total += key * value
        return total

    def roll_rocks(self, tilt_direction: int = 0) -> int:
        self.tilt_map(tilt_direction)
        rolled_rock_data = []
        rock_positions = {x: 0 for x in range(1, len(self.rock_data[0]) + 1)}
        for line in self.rock_data:
            char_list = [char for char in line]
            char_list = self.bubble_sort(char_list)
            for char_ind in range(len(char_list)):
                if char_list[char_ind] == 'O':
                    rock_positions[char_ind + 1] += 1
            rolled_rock_data.append(''.join(char_list))
        return self.calc_load(rock_positions)
        
    def tilt_map(self, direction: int) -> None:
        if direction == 0:
            outer_args = (0, len(self.rock_data[0]), 1)
            inner_args = (len(self.rock_data) - 1, -1, -1)
        elif direction == 1:
            return self.rock_data
        elif direction == 2:
            outer_args = (len(self.rock_data[0]) - 1, -1, -1)
            inner_args = (0, len(self.rock_data), 1)
        elif direction == 3:
            outer_args = (len(self.rock_data) - 1, -1, -1)
            inner_args = (len(self.rock_data[0]) - 1, -1, -1)
        reoriented_data = []
        for outer_ind in range(*outer_args):
            reoriented_line = ""
            for inner_ind in range(*inner_args):
                if direction != 3:
                    reoriented_line += self.rock_data[inner_ind][outer_ind]
                else:
                    reoriented_line += self.rock_data[outer_ind][inner_ind]
            reoriented_data.append(reoriented_line)
        self.rock_data = reoriented_data

        

if __name__ == "__main__":
    rockerfier = RockRoller("day-14/rock-map.txt")

    print(rockerfier.roll_rocks(0))
