from rock_roller import RockRoller

class RockRollerAdvanced(RockRoller):
    def roll_rocks(self, tilt_direction: int = 0) -> int:
        for x in range(1000000000):
            self.tilt_map(tilt_direction)
        # rolled_rock_data = []
        rock_positions = {x: 0 for x in range(1, len(self.rock_data[0]) + 1)}
        for line in self.rock_data:
            # char_list = [char for char in line]
            # char_list = self.bubble_sort(char_list)
            for char_ind in range(len(line)):
                if line[char_ind] == 'O':
                    rock_positions[char_ind + 1] += 1
            # rolled_rock_data.append(''.join(line))
        return self.calc_load(rock_positions)
    
    def tilt_map(self, direction: int) -> None: # Incomplete: too brute-forcey
        for x in range(4): 
            if direction == 0:
                outer_args = (0, len(self.rock_data[0]), 1)
                inner_args = (len(self.rock_data) - 1, -1, -1)
            elif direction == 1:
                return self.rock_data
            elif direction == 2:
                outer_args = (len(self.rock_data[0]) - 1, -1, -1)
                inner_args = (0, len(self.rock_data), 1)
            elif direction == 3:
                outer_args = (len(self.rock_data) - 1, -1, -1)
                inner_args = (len(self.rock_data[0]) - 1, -1, -1)
            reoriented_data = []
            reoriented_data_lists = []
            for outer_ind in range(*outer_args):
                reoriented_line = []
                for inner_ind in range(*inner_args):
                    if direction != 3:
                        reoriented_line.append(self.rock_data[inner_ind][outer_ind])
                    else:
                        reoriented_line.append(self.rock_data[outer_ind][inner_ind])
                reoriented_data_lists.append(reoriented_line)
            for line in reoriented_data_lists:
                tilted_line = self.bubble_sort(line)
                reoriented_data.append(''.join(tilted_line))
            self.rock_data = reoriented_data


if __name__ == "__main__":
    rockadaisy = RockRollerAdvanced("day-14/rock-map-sample-1.txt")

    print(rockadaisy.roll_rocks())
