class CustomClass:
    def __init__(self, file_name: str) -> None:
        with open(file_name, 'r') as file:
            self.subject_data = file.readlines()


if __name__ == "__main__":
    custom_classerifier = CustomClass("day-15/hash-seq-sample-1.txt")
