class CardParser:
    def __init__(self, card_file: str) -> None:
        with open(card_file, 'r') as data:
            self.card_data = data.readlines()

    def string_cleaner(self, line: str) -> list[list[str]]:
        stripped_line = line[10:].removesuffix('\n')
        winning_numbers = stripped_line[:29].split(' ')
        held_numbers = stripped_line[32:].split(' ')
        if len(winning_numbers) > 10:
            diff = len(winning_numbers) - 10
            for ind in range(diff):
                winning_numbers.remove("")
        return [winning_numbers, held_numbers]

    def line_calc(self, line):
        w_nums, h_nums = self.string_cleaner(line)
        line_total = 0
        for ind in range(len(w_nums)):
            if w_nums[ind] in h_nums and line_total == 0:
                line_total = 1
            elif w_nums[ind] in h_nums and line_total >= 1:
                line_total *= 2
        return line_total

    def total_calc(self):
        total = 0
        for ind in range(len(self.card_data)):
            line_total = self.line_calc(self.card_data[ind])
            total += line_total
        return total

parser = CardParser("day-4/scratch-cards.txt")

total = parser.total_calc()

print(total)
