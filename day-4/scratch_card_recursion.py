

class CardParser:
    def __init__(self, card_file: str) -> None:
        with open(card_file, 'r') as data:
            self.card_data = data.readlines()
        self.card_stack = {x: 1 for x in range(len(self.card_data))}
        self.last_match_count = 0


    def string_cleaner(self, line: str) -> list[list[str]]:
        stripped_line = line[10:].removesuffix('\n')
        winning_numbers = stripped_line[:29].split(' ')
        held_numbers = stripped_line[32:].split(' ')
        if len(winning_numbers) > 10:
            diff = len(winning_numbers) - 10
            for ind in range(diff):
                winning_numbers.remove("")
        return [winning_numbers, held_numbers]

    def line_parse(self, line):
        w_nums, h_nums = self.string_cleaner(line)
        self.last_match_count = 0
        for ind in range(len(w_nums)):
            if w_nums[ind] in h_nums:
                self.last_match_count += 1

    def total_calc(self):
        total = 0
        for line_ind in range(len(self.card_data)):
            self.line_parse(self.card_data[line_ind])
            for ind in range(1, self.last_match_count + 1):
                if self.card_stack.get(line_ind + ind) is not None:
                    self.card_stack[line_ind + ind] += self.card_stack[line_ind]
        total = 0
        for value in self.card_stack.values():
            total += value
        return total

parser = CardParser("day-4/scratch-cards.txt")

total = parser.total_calc()

print(total)
print(parser.card_stack)
