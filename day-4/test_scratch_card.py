from scratch_card_calc import CardParser
from unittest import TestCase, mock

class TestParserSuite(TestCase):
    def __init__(self, methodName: str = "runTest") -> None:
        super().__init__(methodName)
        self.parser = self.TestCardParser()
        self.winning_numbers = [
            ["69", "61", "27", "58", "89", "52", "81", "94", "40", "51"],
            ["5", "75", "37", "76", "98", "32", "24", "83", "44", "50"],
            ["37", "39", "43", "53", "47", "20", "50", "56", "78", "65"],
            [ "6", "25", "31", "60", "2", "50", "89", "67", "82", "16"],
            ["43", "39", "98", "45", "33", "87", "36", "23", "61", "66"]
        ]

    class TestCardParser(CardParser):
        def __init__(self) -> None:
            self.card_data = [
                "Card   1: 69 61 27 58 89 52 81 94 40 51 | 43 40 52 90 37 97 89 80 69 42 51 70 94 58 10 73 21 29 61 63 57 79 81 27 35",
                "Card   2:  5 75 37 76 98 32 24 83 44 50 | 80 75 91  5 33 52 31 96 83 92 46 98 55 65 48 24 44  4 32 60 88 37 76 50 77",
                "Card   3: 37 39 43 53 47 20 50 56 78 65 | 80 56 32 78 72 97 40 77 17 50 87 99 36 93 63 19 39 59 44 52 23 75  9 51 43",
                "Card   4:  6 25 31 60  2 50 89 67 82 16 | 65 89  5 67 68 53 50  6 51 25 96 32  2 16 41 60 63 90 82 30 98 31 75 14  7",
                "Card   5: 43 39 98 45 33 87 36 23 61 66 | 93 61  4 21 47 32 94 99 45 23 87 64 92 43 33  5 83 16 98 44 39 77 66 18 36"
            ]

    def test_string_cleaner(self):
        test_line_one = self.parser.string_cleaner(self.parser.card_data[0])
        test_line_two = self.parser.string_cleaner(self.parser.card_data[1])
        test_line_three = self.parser.string_cleaner(self.parser.card_data[2])
        test_line_four = self.parser.string_cleaner(self.parser.card_data[3])
        test_line_five = self.parser.string_cleaner(self.parser.card_data[4])


        self.assertEqual(test_line_one[0], self.winning_numbers[0])
        self.assertEqual(test_line_two[0], self.winning_numbers[1])
        self.assertEqual(test_line_three[0], self.winning_numbers[2])
        self.assertEqual(test_line_four[0], self.winning_numbers[3])
        self.assertEqual(test_line_five[0], self.winning_numbers[4])



    def test_line_calc(self):
            test_line_one = self.parser.line_calc(self.parser.card_data[0])
            test_line_two = self.parser.line_calc(self.parser.card_data[1])
            test_line_three = self.parser.line_calc(self.parser.card_data[2])
            test_line_four = self.parser.line_calc(self.parser.card_data[3])
            test_line_five = self.parser.line_calc(self.parser.card_data[4])

            self.assertEqual(test_line_one, 512)
            self.assertEqual(test_line_two, 512)
            self.assertEqual(test_line_three, 16)
            self.assertEqual(test_line_four, 512)
            self.assertEqual(test_line_five, 512)

    def test_total_calc(self):
        self.parser.line_calc = mock.MagicMock(return_value=1)

        total = self.parser.total_calc()
        self.assertEqual(total, 5)
