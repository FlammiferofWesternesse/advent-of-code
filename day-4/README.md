# Part One Summary

### Method

> For all following challenges, I will be starting with a similar base class, which takes a file name as an argument for `__init__()` and saves the return from `.readlines()` in its state.  

This class has four methods:

1. `__init__()`
2. `string_cleaner()`
3. `line_calc()`
4. `total_calc()`

`__init__()` takes the title of a file as a str, opens it, and stores the list of lines to a state variable.

`string_cleaner()` takes a line, removes unnecessary characters, and returns a list containing two list: `winning_numbers` from the left and `held_numbers` from the right.

`line_calc()` takes a line, and calls line cleaner on it. It then checks if each item in `winning_numbers` appears in `held_numbers`, and adds to the `total` variable, and returns that variable.

`total_calc()` this function uses a simple reductive process on the `card_data` list, calling `line_calc()` on each line. It returns the total of all calls of `line_calc()`.


# Part Two Summary

### Method

For this solution I use the same class and methods.

`__init__()` adds a dictionary and an integer to state, in addition to the `card_data` from part one. The dictionary, `card_stack`, will track the total numbers of each card. The integer, `last_match_count`, is used to hold the number of matches from line_parse, instead of it return a value.

`string_cleaner()` is unchanged.

`line_parse()` calls `string_cleaner()`, resets `last_match_count`, and checks the lists for match, incrementing the match counter.

`total_calc()` iterates through the lines in `card_data`, calls `line_parser()`, and then it iterates through the following loop:

```python
for ind in range(1, self.last_match_count + 1):
    if self.card_stack.get(line_ind + ind) is not None:
        self.card_stack[line_ind + ind] += self.card_stack[line_ind]
```
This loop fulfills the requirement of taking the number of matches and adding the size of the current line's correlated value in `card_stack` to the respective following lines' correlated values. The values of `card_stack` are reduced to a total. `total_calc()` returns this total.
