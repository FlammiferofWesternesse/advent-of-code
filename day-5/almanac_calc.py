"""
Overall strategy:
The following class works seed-by-seed and traces each seed to its conclusion. After
each location has been determined by a serious of logical comparisions in the form:
source_number < seed_number < (source_number + range size)
for each item in the map list, we will have a list to sort and return the lowest int.
"""
from math import inf

class AlmanacCalculator:
    def __init__(self, almanac) -> None:
        with open(almanac, 'r') as almanac_file:
            self.almanac_data: list[str] = almanac_file.readlines()
        self.map_bounds = self.parse_mappings()
        self.seeds_numbers = self.clean_seeds()

    # This function is called by __init__ to parse the first line of the function
    def clean_seeds(self) -> list[int]:
        seeds = self.almanac_data[0]
        seeds = seeds[7:-1].split(' ')
        seeds = [int(item) for item in seeds]
        return seeds

    # This function will iterate through the seed numbers to find the final location of
    # each seed, and will keep track of the lowest only with nearest_location
    def find_location(self) -> int:
        seed_locations = [] # after the last iteration of map_bounds, append(source)
        for seed in self.seeds_numbers:
            source = seed
            for map_bound in self.map_bounds:
                for line_ind in range(map_bound[0], map_bound[1]):
                    read_result = self.read_mapping(source, line_ind)
                    if read_result:
                        difference = source - read_result[1]
                        source = difference + read_result[0]
                        break
            seed_locations.append(source)
        seed_locations.sort()
        return seed_locations[0]

    # This function is called by __init__ and returns a list of "bounds" which will be
    # used to iterate through groups of lines in almanac_data which correspond to mappings
    def parse_mappings(self) -> list[tuple]:
        bounds = []
        start = 0
        for line_ind in range(2, len(self.almanac_data)):
            current_line = self.almanac_data[line_ind]
            if current_line[0].isalpha():
                start = line_ind + 1
            elif current_line[0] == "\n" or line_ind == len(self.almanac_data) - 1:
                bounds.append((start, line_ind))
                start = 0
        return bounds

    # This function takes a source and a line index for almanac data and checks
    # if the source number falls in the mapped range represented
    def read_mapping(self, source_number:int, line_ind: int):
        line = self.almanac_data[line_ind][:-1]
        mapping = [int(elem) for elem in line.split(" ")]
        source_range_start = mapping[1]
        source_range_end = source_range_start + mapping[2]
        if source_range_start <= source_number < source_range_end:
            return mapping
        else:
            return []



if __name__ == "__main__":
    almanator = AlmanacCalculator("day-5/almanac.txt")
    nearest_location = almanator.find_location()
    print(nearest_location)
