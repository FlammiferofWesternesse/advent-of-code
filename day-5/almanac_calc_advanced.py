from almanac_calc import AlmanacCalculator

class AlmanacCalculatorAdvanced(AlmanacCalculator):
    def __init__(self, almanac) -> None:
        super().__init__(almanac)
        self.seed_ranges = self.parse_seed_ranges()

    
    def parse_seed_ranges(self):
        seed_ranges = []
        seed_data = [
            int(number) for number in
            self.almanac_data[0][7:].removesuffix('\n').split(' ')
        ]
        seed_range_start = 0
        seed_range_size = 0
        for ind in range(len(seed_data)):
            if ind % 2 == 0:
                seed_range_start = seed_data[ind]
            else:
                seed_range_size = seed_data[ind]
                seed_range_end = seed_range_start + seed_range_size
                seed_ranges.append((seed_range_start, seed_range_end))
        return seed_ranges

    def find_seed(self):
        seed_location = 0
        eureka = False
        x = 0
        while x < 10000:
            source = seed_location
            for bound_ind in range(len(self.map_bounds) - 1, -1, -1):
                current_bound = self.map_bounds[bound_ind]
                for line_ind in range(current_bound[1] - 1, current_bound[0] - 1, -1):
                    read_result = self.read_mapping_alt(source, line_ind)
                    if read_result:
                        difference = source - read_result[0]
                        source = difference + read_result[1]
                        break

    
    def read_mapping_alt(self, source_number:int, line_ind: int):
        line = self.almanac_data[line_ind][:-1]
        mapping = [int(elem) for elem in line.split(" ")]
        source_range_start = mapping[0]
        source_range_end = source_range_start + mapping[2]
        if source_range_start <= source_number < source_range_end:
            return mapping
        else:
            return []


if __name__ == "__main__":
    almanator = AlmanacCalculatorAdvanced("day-5/almanac.txt")

    print("SEED LOCATION: ", almanator.find_seed())
