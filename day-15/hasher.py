import functools as FT

class Hasher:
    def __init__(self, file_name: str) -> None:
        with open(file_name, 'r') as file:
            self.hash_data = file.readline().removesuffix('\n').split(',')
        print("INITIALIZING: HASH STRING LENGTH: ", len(self.hash_data))

    def hash_sequence(self, accum: int | str, string: str):
        total = 0
        for char in string:
            total += ord(char)
            total *= 17
            total %= 256
        if type(accum) == str:
            accum_total = 0
            for char in accum:
                accum_total += ord(char)
                accum_total *= 17
                accum_total %= 256
            return total + accum_total
        return total + accum
    
    def sum_hash_sequences(self):
        result = FT.reduce(self.hash_sequence, self.hash_data)
        return result


if __name__ == "__main__":
    hash_browner = Hasher("day-15/hash-seq.txt")

    # print(hash_browner.sum_hash_sequences())  
    print(hash_browner.hash_sequence(0, 'cm'))
