from hasher import Hasher
from typing import Union
import functools as FT

class HashBrowner(Hasher):
    def __init__(self, file_name: str) -> None:
        super().__init__(file_name)
        self.lens_boxes = {x: {} for x in range(256)}

    def simple_hash_sequence(self, string: str):
        total = 0
        for char in string:
            total += ord(char)
            total *= 17
            total %= 256
        return total
    
    def label_reader(self, label: str) -> tuple[Union[int, str]]:
        if label[-1] == '-':
            label_name = label[:-1]
            focal_length = None
        else:
            label_name = label[:-2]
            focal_length = int(label[-1])
        box_num = self.simple_hash_sequence(label_name)
        return (box_num, label_name, focal_length)

    def focus_calc(self) -> int:
        focus_intensity = 0
        for label in self.hash_data:
            lens_info = self.label_reader(label)
            if lens_info[2] is not None:
                new_lens = {lens_info[1]: lens_info[2]}
                self.lens_boxes[lens_info[0]].update(new_lens)
            else:
                if self.lens_boxes[lens_info[0]].get(lens_info[1]) is not None:
                    del self.lens_boxes[lens_info[0]][lens_info[1]]
        for key in self.lens_boxes.keys():
            focus_intensity += self.lens_calc(key, self.lens_boxes[key])
        return focus_intensity

    def lens_calc(self, box_num: int, box: dict):
        if box == {}:
            return 0
        box_num += 1
        box_total = 0
        lens_slot = 1
        for value in box.values():
            box_total += value * box_num * lens_slot
            lens_slot += 1
        return box_total



if __name__ == "__main__":
    hash_browner = HashBrowner("day-15/hash-seq.txt")

    print(hash_browner.focus_calc())  
    # print(hash_browner.hash_sequence(0, 'cm'))
