# Part One Summary

### Method

The overall strategy here is simple: store the `directions` string and the `map_nodes` list in the class object's state, then start an indefinite while loop and a nested for loop to iterate through `directions`. Traverse the nodes like a binary tree until reaching the destination. Count each iteration of the for loop and return that total as the solution. I only required three methods to accomplish this:

1. `__init__(self, file_name) -> None`
2. `parse_map(self) -> dict`
3. `solve_map(self) -> int`

`__init__`, aside from the regular document reading, takes the first line of the map to save it as `directions` and calls `parse_map` to save it as `map_nodes`

`parse_map` iterates through each dictionary style line of `map_data`, puts it into Python dictionary form in `map_nodes`, and returns `map_nodes`.

`solve_map` opens a while loop which opens a for loop to iterate through `directions`, counting each iteration of the for loop. It checks if the node it is stepping into is "ZZZ", breaks both loops if it is, and moves on to the next if it isn't. Returns the iteration counter.

# Part Two Summary

### Method

Although incomplete because I am not sure how to work around Python's number limits, my solution had involved using a modified version of the `solve_map` algorithm for each starting node, and then finding the least common multiple of all nodes. At smaller scales this worked, but it broke down as the numbers got ridiculously high. I shall attempt to refine my solution later. 
