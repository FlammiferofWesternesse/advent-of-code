"""
Overall Strategy: This shall follow a similar process to the original script.
The primary difference is that I shall need to substitute a single node variable with 
with a data structure starting with all valid nodes.
"""
from map_reading import MapReader
from numpy import lcm

class MapReaderGhost(MapReader):
    def __init__(self, file_name: str) -> None:
        super().__init__(file_name)
        self.starting_nodes = self.select_starting_nodes()

    # Due time time, this method is incomplete: the numpy int32 data type cannot hold 
    # the result of lcm(1803075805, 17141), returning -62288511 instead, so my current 
    # solution, while valid for smaller numbers, is not appropriate for larger problem. 
    # While the function does complete, the current given solution of 1012801613 is too 
    # small. I will complete this challenge later.
    def find_lcm(self, iterations: list) -> int:
        print("BEGINNING LCM")
        i = 10
        while len(iterations) > 1:
            print(iterations)
            new_lcm = [lcm(iterations[0], iterations[1])]
            if len(iterations) == 2:
                result = new_lcm[0]
                break
            else:
                iterations = new_lcm + iterations[2:]
        return result
    
    def select_starting_nodes(self) -> list[str]:
        starting_nodes = []
        for node_name, node_links in self.map_nodes.items():
            if node_name[2] == "A":
                starting_nodes.append(node_name)
        return starting_nodes

    def solve_ghost_map(self) -> int:
        node_iterations = []
        nodes = self.starting_nodes
        # call solve_map on each starting node
        for node in nodes:
            node_iterations.append(self.solve_ghost_node(node))

        # find the lowest common denominator
        found = False
        least_common_multiple = self.find_lcm(node_iterations)
        
        return least_common_multiple
                        
    def solve_ghost_node(self, node) -> int:
        iteration = 0
        node = self.map_nodes[node]
        arrived = False
        while not arrived:
            for char in self.directions:
                iteration += 1
                if node[char][2] == "Z":
                    arrived = True
                    break
                node = self.map_nodes[node[char]]
        return iteration

if __name__ == "__main__":
    map_readerifier = MapReaderGhost('day-8/map.txt')

    print(map_readerifier.solve_ghost_map())
