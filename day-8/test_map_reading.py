from unittest import TestCase, mock
from map_reading import MapReader

class TestMapReader(TestCase, MapReader):
    
    
    def test_parse_map(self):
        self.map_data = [
            'RL', 
            '', 
            'AAA = (BBB, CCC)', 
            'BBB = (DDD, EEE)', 
            'CCC = (ZZZ, GGG)', 
            'DDD = (DDD, DDD)', 
            'EEE = (EEE, EEE)', 
            'GGG = (GGG, GGG)', 
            'ZZZ = (ZZZ, ZZZ)'
        ]

        sample_nodes = {
        'AAA': {'L': 'BBB', 'R': 'CCC'}, 
        'BBB': {'L': 'DDD', 'R': 'EEE'}, 
        'CCC': {'L': 'ZZZ', 'R': 'GGG'}, 
        'DDD': {'L': 'DDD', 'R': 'DDD'}, 
        'EEE': {'L': 'EEE', 'R': 'EEE'}, 
        'GGG': {'L': 'GGG', 'R': 'GGG'}, 
        'ZZZ': {'L': 'ZZZ', 'R': 'ZZZ'}
        }
        
        self.assertEqual(self.parse_map(), sample_nodes)
        

    def test_solve_map(self):
        self.directions = "RL"
        self.map_nodes = {
        'AAA': {'L': 'BBB', 'R': 'CCC'}, 
        'BBB': {'L': 'DDD', 'R': 'EEE'}, 
        'CCC': {'L': 'ZZZ', 'R': 'GGG'}, 
        'DDD': {'L': 'DDD', 'R': 'DDD'}, 
        'EEE': {'L': 'EEE', 'R': 'EEE'}, 
        'GGG': {'L': 'GGG', 'R': 'GGG'}, 
        'ZZZ': {'L': 'ZZZ', 'R': 'ZZZ'}
        }

        self.assertEqual(self.solve_map(), 2)