"""
Overall Strategy: Parse each node into a dictionary with "left" and "right" nodes.
Start a while loop with a nested for loop iterating through the directions string.
break the while loop when 'ZZZ' is hit.
"""

class MapReader:
    def __init__(self, file_name: str) -> None:
        with open(file_name) as file:
            self.map_data = [line.removesuffix('\n') for line in file.readlines()]
        self.directions = self.map_data[0].removesuffix('\n')
        self.map_nodes = self.parse_map()

    # This method parses the source document data into a pythonic dictionary format and
    # saves it in the class objects state.
    def parse_map(self) -> dict:
        map_nodes = {}

        for line_ind in range(2, len(self.map_data)):
            line = self.map_data[line_ind]
            map_nodes[line[0:3]] = {"L": line[7:10], "R": line[12:15]}

        return map_nodes

    # This method opens a while loop to start a nested for loop until the for loop 
    # reaches the solution. In its current state, the loop is potentially infinite.
    def solve_map(self) -> int:
        iteration = 0
        node = self.map_nodes["AAA"]
        arrived = False
        while not arrived:
            for char in self.directions:
                iteration += 1
                if node[char] == "ZZZ":
                    arrived = True
                    break
                node = self.map_nodes[node[char]]
        return iteration


if __name__ == "__main__":
    map_readerifier = MapReader('day-8/map.txt')

    print(map_readerifier.solve_map())
