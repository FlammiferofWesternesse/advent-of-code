class MirrorDiagramReader:
    def __init__(self, file_name) -> None:
        with open(file_name, 'r') as mirror_file:
            self.mirror_data = mirror_file.readlines()

    def columnwise_symetricality_check(
            self, mirror_positions: list[list[int]]
    ) -> int | None:
        pass
        mirror_positions = self.linewise_to_columnwise(mirror_positions)
        return self.symetricality_check(mirror_positions)
    
    def linewise_to_columnwise(self, mirror_positions: list[str]) -> list:
        new_mirror_positions = []
        for char_ind in range(len(mirror_positions[0])):
            new_str = ""
            for line_ind in range(len(mirror_positions)):
                new_str += mirror_positions[line_ind][char_ind]
            new_mirror_positions.append(new_str)
        return new_mirror_positions

    def symetricality_check(self, mirror_positions: list[str]) -> int | None:
        symmetry_start = None
        symmetry_end = None
        center = None
        x = 1
        for i in range(len(mirror_positions)):
            # if (
            #     mirror_positions[i] == mirror_positions[-1] and
            #     symmetry_start is None
            # ):
            # print(
            #     "symmetry_start".upper(), symmetry_start, '\n', 
            #     "symmetry_end".upper(), symmetry_end, '\n',
            #     'X', x, '\n'
            # )
            if symmetry_start is None:
                    for j in range(len(mirror_positions) - 1, i, -1):
                        if mirror_positions[i] == mirror_positions[j]:
                            symmetry_start = i
                            symmetry_end = j
                            break
                    continue
            elif i + 1 == symmetry_end - x:
                center = symmetry_end - x
                symmetry_size = symmetry_end - symmetry_start
                break
            elif mirror_positions[i] == mirror_positions[symmetry_end - x]:
                x += 1
            # elif mirror_positions[symmetry_start] != mirror_positions[symmetry_end - x]:
            #     symmetry_start = None
            #     symmetry_end = None
            #     x = 1

            # if (
            #     mirror_positions[i] in mirror_positions[i + 1:] and
            #     symmetry_start is not None
            # ):
            #     symmetry_end = mirror_positions.index(mirror_positions[i], i + 1)
            #     if i + 1 == symmetry_end:
            #         center = i + 1
            #         break
            # elif symmetry_start is not None:
            #     symmetry_start = None
        return (center, symmetry_size)

    def mirror_summary(self):
        symmetries = {"line": [], "column": []}
        partitions = self.partition_images()
        for partition in partitions:
            mirror_positions = [
                line.removesuffix('\n') for line in self.mirror_data[
                    partition[0]:partition[1]
                ]
            ]
            line_symmetry_start = self.symetricality_check(mirror_positions)
            column_symmetry_start = self.columnwise_symetricality_check(
                mirror_positions
            )
            print(line_symmetry_start, column_symmetry_start)
            if line_symmetry_start[1] > column_symmetry_start[1]:
                symmetry = ["line", line_symmetry_start[0]]
            else:
                symmetry = ["column", column_symmetry_start[0]]
            symmetries[symmetry[0]].append(symmetry[1])
            # if line_symmetry_start is not None:
            #     print(partition, line_symmetry_start) 
            #     symmetries['line'].append(line_symmetry_start)
            # if column_symmerry_start is not None:
            #     print(partition, column_symmerry_start)
            #     symmetries['column'].append(column_symmerry_start)
        print(symmetries)
        return (sum(symmetries['line']) * 100) + sum(symmetries['column'])
        # return symmetries

    def parse_image(self, start: int, end: int):
        mirror_positions = []
        for line_ind in range(start, end):
            line = self.mirror_data[line_ind]
            line_mirror_positions = []
            for char_ind in range(len(line)):
                if line[char_ind] == '#':
                    line_mirror_positions.append(char_ind)
            mirror_positions.append(line_mirror_positions)
        return mirror_positions
              
    def partition_images(self):
        partitions = []
        start = 0
        for line_ind in range(len(self.mirror_data)):
            if self.mirror_data[line_ind] in ('\n', ''):
                partitions.append((start, line_ind))
                start = line_ind + 1
            elif line_ind + 1 == len(self.mirror_data):
                partitions.append((start, line_ind + 1))
        for partition in partitions:
            print(partition)
        return partitions


if __name__ == "__main__":
    mirrororororator = MirrorDiagramReader('day-13/mirror-pattern-diagram-sample-1.txt')

    print(mirrororororator.mirror_summary())
            
