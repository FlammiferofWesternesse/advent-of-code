from unittest import TestCase, mock
from pipe_tracer import PipeTracer


class TestPipeTracer(TestCase, PipeTracer):
    def test_parse_matrix(self):
        self.file_data = [
            "..F7.\n",
            ".FJ|.\n",
            "SJ.L7\n",
            "|F--J\n",
            "LJ...\n"
        ]
        pipe_matrix = [
            "..F7.",
            ".FJ|.",
            "SJ.L7",
            "|F--J",
            "LJ..."
        ]


        result = self.parse_matrix()

        self.assertEqual(self.S_location, (0, 2))
        self.assertEqual(self.parse_matrix(), pipe_matrix)

    def test_find_S_connections(self):
        self.S_location = (0, 2)
        self.pipe_matrix = [
            "..F7.",
            ".FJ|.",
            "SJ.L7",
            "|F--J",
            "LJ..."
        ]

        result = self.find_S_connections()

        self.assertEqual(result, [1, 2, 3])

    def test_trace_pipe_circuit(self):
        pipe_matrices = [
            [
                ".....",
                ".S-7.",
                ".|.|.",
                ".L-J.",
                ".....",
            ],
            [
                "..F7.",
                ".FJ|.",
                "SJ.L7",
                "|F--J",
                "LJ..."
            ],
            [
                ".F--7",
                "FJ.FJ",
                "|..L7",
                "L--7|",
                "...SJ",
            ]
        ]
        S_locations = [
            (1, 1),
            (0, 2),
            (3, 4),
        ]
        self.pipe_codes = {
            'J': ((-1, 3, 1), 0, 0, (-1, 0, 2)),
            '7': (0, 0, (-1, 3, 1), (1, 2, 0)),
            'L': ((1, 1, 3), (-1, 0, 2)),
            'F': (0, (1, 2, 0), (1, 1, 3)),
            '|': ((1, 2, 0), 0, (-1, 0, 2)),
            '-': (0, (-1, 3, 1), 0, (1, 1, 3))
        }


        results = []
        for ind in range(3):
            self.pipe_matrix = pipe_matrices[ind]
            self.S_location = S_locations[ind]
            results.append(self.trace_pipe_circuit())

        self.assertEqual(results[0], 4)
        self.assertEqual(results[1], 8)
        self.assertEqual(results[2], 9)
