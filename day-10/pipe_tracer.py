"""
Overall Strategy: this one got a little messy, but the general approach is to find 'S', 
and then enter a while loop (assuming the problem has a valid answer) to look at the 
next character in the progression by using a dictionary with tuples indicating the 
values by which to move.
In this solution, all directions are respresented as N=0, E=1, S=2, W=3.
"""

class PipeTracer:
    def __init__(self, file_name: str) -> None:
        with open(file_name, 'r') as file:
            self.file_data = file.readlines()
        self.pipe_matrix = self.parse_matrix()
        # pipe_codes creates a mapping so that each character has a pair of tuples, at 
        # position whose index respresents a from_direction 
        self.pipe_codes = {
            'J': ((-1, 3, 1), 0, 0, (-1, 0, 2)),
            '7': (0, 0, (-1, 3, 1), (1, 2, 0)),
            'L': ((1, 1, 3), (-1, 0, 2)),
            'F': (0, (1, 2, 0), (1, 1, 3)),
            '|': ((1, 2, 0), 0, (-1, 0, 2)),
            '-': (0, (-1, 3, 1), 0, (1, 1, 3))
        }

    # This method finds which direction to start from 'S', assuming there are only two 
    # pipes leading away from it.
    def find_S_connections(self) -> list:
        pipe_location = list(self.S_location)
        if pipe_location[1] > 0:
            north_pipe = self.pipe_matrix[pipe_location[1] - 1][pipe_location[0]]
        if pipe_location[1] < len(self.pipe_matrix) - 1:
            south_pipe = self.pipe_matrix[pipe_location[1] + 1][pipe_location[0]]
        if pipe_location[0] < len(self.pipe_matrix[pipe_location[1]]):
            east_pipe = self.pipe_matrix[pipe_location[1]][pipe_location[0] + 1]
        if north_pipe == '7' or north_pipe == 'F' or north_pipe == '|':
            return [pipe_location[0], pipe_location[1] - 1, 2]
        elif east_pipe == '7' or east_pipe == 'J' or east_pipe == '-':
            return [pipe_location[0] + 1, pipe_location[1], 3]
        elif south_pipe == 'J' or south_pipe == 'L' or south_pipe == '|':
            return [pipe_location[0], pipe_location[1] + 1, 0]
        
        
    # This parser creates the regular list of cleaned up strings and finds 'S'
    def parse_matrix(self) -> list[str]:
        map_list = []
        for line_ind in range(len(self.file_data)):
            S = self.file_data[line_ind].find('S')
            if S != -1:
                self.set_S((S, line_ind))
            map_list.append(self.file_data[line_ind].removesuffix('\n'))
        return map_list
    
    # This method just changes S_location in state
    def set_S(self, coords: tuple) -> None:
        self.S_location = coords

    # Gets the first post-S pipe and iterates from there. The tuple assign to pipe has 
    # the following format: x coord, y coord, from_direction the pipe_name (the letter 
    # in the matrix) is used with the from direct to figure out which change tuple to 
    # use in pipe_codes. The first figure in the tuple repesents an amount to change,
    # the second represented the from direction, and the third represents the to 
    # direction.
    def trace_pipe_circuit(self) -> int:
        pipe = self.find_S_connections() # find which none-S pipe to start from
        step_count = 1
        while True:
            from_direction = pipe[2]
            pipe_location = pipe[0:2]
            pipe_name = self.pipe_matrix[pipe_location[1]][pipe_location[0]]
            change = self.pipe_codes[pipe_name][from_direction]
            if change[1] == 0 or change[1] == 2:
                pipe_location[1] = pipe_location[1] + change[0]
            else:
                pipe_location[0] = pipe_location[0] + change[0]
            step_count += 1
            pipe = pipe_location + [change[2]]
            if self.pipe_matrix[pipe_location[1]][pipe_location[0]] == 'S':
                break    
        return step_count // 2
                 

if __name__ == "__main__":
    tracerator = PipeTracer('day-10/pipe-map.txt')

    print(tracerator.trace_pipe_circuit())
