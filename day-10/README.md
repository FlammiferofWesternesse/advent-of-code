# Part One Summary

### Method

In this problem, my solution assumes that the circuit of pipes that 'S' connects only has 1 pipe going in to 'S' and 1 pipe going out. In other words, in the up, down, left, and right dimensions, 'S' will only have two connections, which can be traced.
Overall strategy: Starting from 'S', use a dictionary to map 'L', '7', 'F', 'J', '-', and '|' to changes in matrix coordinates.

I achieved my solution with five methods:

1. `__init__(self, file_name: str) -> None`
2. `find_S_connections(self) -> list`
3. `parse_matrix(self) -> list[str]`
4. `set_S(self, coords: tuple) -> None`
5. `trace_pipe_circuit(self) -> int`

`__init__` sets up the file data for use, calls parse_matrix, and stores a mapping in state for the characters in the matrix. Returns None

`find_S_connections` finds the first pipe in the sequence after S. Returns a tuple in the form x coord, y coord, from_direction.

`parse_matrix` cleans the strings from the file_data and finds the location of 'S' and calls `set_S`.

`set_S` simply takes the coordinates given and writes them as a tuple to state.

`trace_pipe_circuit` iterates through the pipe circuit, using the pipe letters and the info from `find_S_connections` to determine which pipe to go to next. Increments step_count on each iteration and, when it reaches 'S' again, returns `step_count // 2`.


# Part Two Summary

### Method
