# Part One Summary

### Method

My solution to this problem involves scanning the whole document for blank rows and columns. Blank row and column indices are saved so that we can iterate through again, and save the coordinates to a dictionary with the required spacing. This is the first problem where I started using itertools and functools for my reductions instead of itertions.
[NOTE: I realized as I wrote this that I could easily have incorporated these to iterations together. I may return and do that later.]
With the coordinates saved, we take all combinations of them and reduce the differences to find our solution.
I used the following four methods for this:

1. `__init__(self, file_name: str) -> None`
2. `calc_galaxies(self) -> int`
3. `expand_galaxy(self) -> dict`
4. `parse_galaxies(self, galaxy_list: list[str], blank_rows: list[int], blank_columns: list[int]) -> dict`

`__init__` sets up our file data and calls `expand_galaxy`.

`calc_galaxies` uses `combinations` and `reduce` to take the coordinates in `galaxy_dict` and sum up the distances between them. Returns the result as a int

`expand_galaxy` identifies the blank rows and columns. Returns the dict from `parse_galaxies`.

`parse_galaxies` takes the lists of blanks and scans the doc again for galaxies, writing the blank-adjusted coordinates into a dict. Returns that dict.

# Part Two Summary

### Method

I only needed to change two lines in `parse_galaxies` to complete part two. See lines 23 and 32.
