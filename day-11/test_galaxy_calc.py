"""
In these 9 galaxies, there are 36 pairs. Only count each pair once; order within the 
pair doesn't matter. For each pair, find any shortest path between the two galaxies 
using only steps that move up, down, left, or right exactly one . or # at a time. (The 
shortest path between two galaxies is allowed to pass through another galaxy.)

For example, here is one of the shortest paths between galaxies 5 and 9:
....1........
.........2...
3............
.............
.............
........4....
.5...........
.##.........6
..##.........
...##........
....##...7...
8....9.......  
This path has length 9 because it takes a minimum of nine steps to get from galaxy 5 to 
galaxy 9 (the eight locations marked # plus the step onto galaxy 9 itself). Here are 
some other example shortest path lengths:

Between galaxy 1 and galaxy 7: 15
Between galaxy 3 and galaxy 6: 17
Between galaxy 8 and galaxy 9: 5
In this example, after expanding the universe, the sum of the shortest path between all 
36 pairs of galaxies is 374.
"""


from unittest import TestCase, mock
from galaxy_calc import GalaxyCalculator
from galaxy_calc_but_bigger import SuperGalaxyCaclculator

class TestGalaxyCalculator(TestCase, GalaxyCalculator):
    def test_expand_galaxies(self):
        self.galaxy_data = [
            "...#......\n",
            ".......#..\n",
            "#.........\n",
            "..........\n",
            "......#...\n",
            ".#........\n",
            ".........#\n",
            "..........\n",
            ".......#..\n",
            "#...#.....\n",
        ]
        galaxy_list = [
            "...#......",
            ".......#..",
            "#.........",
            "..........",
            "......#...",
            ".#........",
            ".........#",
            "..........",
            ".......#..",
            "#...#.....",
        ]
        galaxy_dict = {
                0: (4, 0),
                1: (9, 1),
                2: (0, 2),
                3: (8, 5),
                4: (1, 6),
                5: (12, 7),
                6: (9, 10),
                7: (0, 11),
                8: (5, 11),
        }
        self.parse_galaxies = mock.MagicMock(
            return_value={
                0: (4, 0),
                1: (9, 1),
                2: (0, 2),
                3: (8, 5),
                4: (1, 6),
                5: (12, 7),
                6: (9, 10),
                7: (0, 11),
                8: (5, 11),
            }
        )
        blank_rows = [3, 7]
        blank_columns = [2, 5, 8]
        
        results = self.expand_galaxy()

        self.assertDictEqual(results, galaxy_dict)
        self.assertListEqual(self.parse_galaxies.call_args[0][0], galaxy_list)
        self.assertListEqual(self.parse_galaxies.call_args[0][1], blank_rows)
        self.assertListEqual(self.parse_galaxies.call_args[0][2], blank_columns) 
    
    def test_parse_galaxy(self):
        galaxy_list = [
            "...#......",
            ".......#..",
            "#.........",
            "..........",
            "......#...",
            ".#........",
            ".........#",
            "..........",
            ".......# ..",
            "#...#.....",
        ]
        blank_rows = [3, 7]
        blank_columns = [2, 5, 8]

        expected_coords = {
            0: (4, 0),
            1: (9, 1),
            2: (0, 2),
            3: (8, 5),
            4: (1, 6),
            5: (12, 7),
            6: (9, 10),
            7: (0, 11),
            8: (5, 11),
        }

        result = self.parse_galaxies(galaxy_list, blank_rows, blank_columns)

        self.assertDictEqual(result, expected_coords)


    def test_calc_galaxies(self):
        self.galaxy_dict = {
            0: (4, 0),
            1: (9, 1),
            2: (0, 2),
            3: (8, 5),
            4: (1, 6),
            5: (12, 7),
            6: (9, 10),
            7: (0, 11),
            8: (5, 11),
        }
        
        result = self.calc_galaxies()

        self.assertEqual(result, 374)
        

class TestGalaxyCalculator(TestCase, SuperGalaxyCaclculator):
    def test_parse_galaxies(self):
        galaxy_list = [
            "............#........................",
            ".........................#...........",
            "#....................................",
            ".....................................",
            ".....................................",
            ".....................................",
            ".....................................",
            ".....................................",
            ".....................................",
            ".....................................",
            ".....................................",
            ".....................................",
            ".....................................",
            "........................#............",
            ".#...................................",
            "....................................#",
            ".....................................",
            ".....................................",
            ".....................................",
            ".....................................",
            ".....................................",
            ".....................................",
            ".....................................",
            ".....................................",
            ".....................................",
            ".....................................",
            ".........................#...........",
            "#............#.......................",
        ]
