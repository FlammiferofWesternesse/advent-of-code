"""
Overall Strategy: I think it will be simplest to just create a dictionary with each 
galaxy arbitrarily numbered as the key, and its x, y coords as a value in a tuple.
"""
import itertools as IT
import functools as FT


class GalaxyCalculator:
    def __init__(self, file_name: str) -> None:
        with open(file_name, 'r') as file:
            self.galaxy_data = file.readlines()
        self.galaxy_dict = self.expand_galaxy()

    # This function takes all combinations of coordinate pairs and uses reduce to sum 
    # their differences
    def calc_galaxies(self) -> int:
        coord_pairs = [pair for pair in IT.combinations(self.galaxy_dict.values(), 2)]
        result = FT.reduce(lambda accum, vals: (
            accum + abs(vals[1][0] - vals[0][0]) + abs(vals[1][1] - vals[0][1])
        ), coord_pairs, 0)
        return result

    # Store the blank values, and feed them to parse_palaxies to write the expanded 
    # coordinates in a dict. Returns the parse_galaxies call.
    def expand_galaxy(self) -> dict:
        galaxy_list = [line.removesuffix('\n') for line in self.galaxy_data]
        blank_rows = []
        blank_columns = []
        
        # Read for blank rows by comparing current line to an equal length line of dots.
        for line_ind in range(len(galaxy_list)):
            line = galaxy_list[line_ind]
            blank = '.' * len(line)
            if line == blank:
                blank_rows.append(line_ind)

        # Read for blank columns
        for char_ind in range(len(galaxy_list[0])):
            blank = True
            for line_ind in range(len(galaxy_list)):
                line = galaxy_list[line_ind]
                char = line[char_ind]
                if char != '.':
                    blank = False
                    break
            if blank:
                blank_columns.append(char_ind)
        
        return self.parse_galaxies(galaxy_list, blank_rows, blank_columns)

    # Take the list of lines and the indices of blank rows/columns, iterate through 
    # each line and character of the document, and adjust the actual index variables
    # according to how many blank rows/columns have been passed. Write the galaxy coords
    # to a dict with these adjusted numbers. Returns that dict.
    def parse_galaxies(
            self, 
            galaxy_list: list[str], 
            blank_rows: list[int], 
            blank_columns: list[int]
    ) -> dict:
        # blank_row_ind is used to adjust the y coordinate for galaxies, 
        # next_blank_row is used to check when blank_row_ind needs to be increased. 
        blank_row_ind = 0
        next_blank_row = blank_rows[blank_row_ind]
        galaxy_dict = {}
        for line_ind in range(len(galaxy_list)):
            line = galaxy_list[line_ind]

            # Check to see if current row has passed the next blank row and isn't maxed 
            # out yet.
            if line_ind > next_blank_row and blank_row_ind != len(blank_rows):
                blank_row_ind += 1
                if blank_row_ind < len(blank_rows):
                    next_blank_row = blank_rows[blank_row_ind]
            actual_line_ind = line_ind + blank_row_ind
            
            # Repeat the above processes for each column
            blank_column_ind = 0
            next_blank_column = blank_columns[blank_column_ind]
            for char_ind in range(len(line)):
                char = line[char_ind]
                if char_ind > next_blank_column and blank_column_ind != len(blank_columns):
                    blank_column_ind += 1
                    if blank_column_ind < len(blank_columns):
                        next_blank_column = blank_columns[blank_column_ind]
                actual_char_ind = char_ind + blank_column_ind

                # When a galaxy is found, write it to the dictionary with the modified
                # coordinates
                if char == '#':
                    galaxy_dict[len(galaxy_dict)] = (actual_char_ind, actual_line_ind)
        return galaxy_dict
        

if __name__ == "__main__":
    galactator = GalaxyCalculator('day-11/galaxy-map-sample-1.txt')

    print(galactator.calc_galaxies())
