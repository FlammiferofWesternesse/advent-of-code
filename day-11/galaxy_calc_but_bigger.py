from galaxy_calc import GalaxyCalculator

class SuperGalaxyCaclculator(GalaxyCalculator):

    # This solution only needed two changes from the original on lines 23 and 32.
    # the actual index formula is changed to multiply the blank list index by 
    # 1000000, then subtract the blank index count itself.
    def parse_galaxies(
            self, 
            galaxy_list: list[str], 
            blank_rows: list[int], 
            blank_columns: list[int]
    ) -> dict:
        blank_row_ind = 0
        next_blank_row = blank_rows[blank_row_ind]
        galaxy_dict = {}
        for line_ind in range(len(galaxy_list)):
            line = galaxy_list[line_ind]
            if line_ind > next_blank_row and blank_row_ind != len(blank_rows):
                blank_row_ind += 1
                if blank_row_ind < len(blank_rows):
                    next_blank_row = blank_rows[blank_row_ind]
            actual_line_ind = line_ind + (blank_row_ind * 1000000) - blank_row_ind
            blank_column_ind = 0
            next_blank_column = blank_columns[blank_column_ind]
            for char_ind in range(len(line)):
                char = line[char_ind]
                if char_ind > next_blank_column and blank_column_ind != len(blank_columns):
                    blank_column_ind += 1
                    if blank_column_ind < len(blank_columns):
                        next_blank_column = blank_columns[blank_column_ind]
                actual_char_ind = char_ind + (blank_column_ind * 1000000) - blank_column_ind
                if char == '#':
                    galaxy_dict[len(galaxy_dict)] = (actual_char_ind, actual_line_ind)
        return galaxy_dict

if __name__ == "__main__":
    galactator = SuperGalaxyCaclculator('day-11/galaxy-map.txt')

    print(galactator.calc_galaxies())
