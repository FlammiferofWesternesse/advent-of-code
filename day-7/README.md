# Part One Summary

### Method

My solution uses a prefined ranks dictionary with as many ranks as there are lines and stores dicts as values, and a cad_values dictionary that maps card letters to numeric values for the sake of comparison.

Each hand is parsed, during which the cards, wager, and strength are put in a dictionary.

The hand is then assigned a rank based on its strength. The `set_rank` function is called recursively to shift the ranks when necessary.

In a reductive process, the value of each hand's wager is mulitplied by its rank number. All totals are summed up for the problem's solution.

This solution uses a class that has two unique state variables: `ranks` and `card_values`. It also has six methods:

1. `__init__(self, filename: str) -> None`
2. `strength_test(self, set_length: int, hand_cards: str) -> int:`
3. `hands_calc(self) -> int`
4. `parse_hand(self, line_ind: int) -> dict`
5. `set_ranks(self, hand: dict) -> None`
6. `shift_rank(self, rank: int, hand: dict)`

`__init__` takes a filename string as an argument. It sets the state variables mentioned above, as well as the hand_data list. Returns None.

`strength_test` takes `set_length` and `hand_cards` as arguments. Set length represents the number of unique cards in each hand. From this number, we can deduce one or two possibilities for hand strength, and verify with another test. Returns int.

`hands_calc` is the base method in this script, which iterates through the lines, calls the parser, calls `set_rank`, and does the reduction to get the final score. Returns int.

`parse_hand` takes a line index as its argument, puts the cards and wager from that line into a pair of variables, gets the length of `set(hand_cards)`, and calls `strength_test`. Returns the following dictionary:
```python
{
    "hand_cards": hand_cards,
    "first_card": hand_cards[0],
    "second_card": hand_cards[1],
    "third_card": hand_cards[2],
    "fourth_card": hand_cards[3],
    "fifth_card": hand_cards[4],
    "hand_strength": hand_strength, 
    "hand_wager": int(hand_wager)
}
```
I chose to have a longer-form dictionary to make `set_ranks` look a little more readable when it is checking individual card values.

`set_ranks()` takes a hand, and iterates through each rank in `ranks`. If  there is no dictionary at a given rank, it stores hand there. If there is a hand dictionary, and it begins comparing hand strengths and individual card strengths. If `hand` is weaker than the hand in the current rank, then it calls `shift_rank`. Returns None.

`shift_rank` takes a rank int and a hand dictionary. It saves the current hand stored in `self.ranks[rank]`, overwrites the rank with `hand`, and if there is a card in the next rank calls itself with `(rank  + 1, old_hand)`, otherwise it calls `set_ranks(old_hand)`.

# Part Two Summary

### Method
