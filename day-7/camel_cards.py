"""
Overall Strategy:
To parse the hands, we can use the set() constructor to turn each hand into a set, then 
count the number of occurances of each character if the lengths of the hand and the set
are unequal.
For ranking, we could put each and into a dictionary with existing fields for 1-1000, 
and whenever a new card if added, slot it in and recusively change the cards above it.
"""


class HandScorer:
    # In our constructor, we create two extra state variables: ranks and card_values.
    # ranks will be used to order each hand by ranks, and cad_values provides a mapping
    # to compare the value of each card.
    def __init__(self, filename: str) -> None:
        with open(filename) as hands_file:
            self.hands_data = [line.removeprefix('\n') for line in hands_file.readlines()]
        self.ranks = {x:{} for x in range(1, len(self.hands_data) + 1)}
        self.card_values = {
            "A": 13,
            "K": 12,
            "Q": 11,
            "J": 10,
            "T": 9,
            "9": 8,
            "8": 7,
            "7": 6,
            "6": 5,
            "5": 4,
            "4": 3,
            "3": 2,
            "2": 1,
        }

        
    # Using the set length, we can make our comparisons simpler; if the set of unique 
    # characters from hand_cards has a length of three, then we can deduce that it is
    # either two pair or three-of-a-kind, because those are the only configurations
    # available with five cards of three types. This method follows that logic for 
    # all possible subsets.
    def strength_test(self, set_length: int, hand_cards: str) -> int:
        if set_length != len(hand_cards):
            if set_length == 1:
                hand_strength = 6
            elif set_length == 2:
                if (
                    hand_cards.count(hand_cards[0]) == 4 or 
                    hand_cards.count(hand_cards[0]) == 1
                ):
                    hand_strength = 5
                else:
                    hand_strength = 4
            elif set_length == 3:
                if (
                    hand_cards.count(hand_cards[0]) == 3 or
                    hand_cards.count(hand_cards[1]) == 3 or
                    hand_cards.count(hand_cards[2]) == 3
                ):
                    hand_strength = 3
                else:
                    hand_strength = 2
            else:
                hand_strength = 1
            return hand_strength
        else:
            return 0

    # This is the base method which iterates through lines of the file to get each 
    # hand parsed and ranked, and then it iterates through the ranks to total out the
    # grand_total and returns it.
    def hands_calc(self) -> int:
        for line_ind in range(len(self.hands_data)):
            hand = self.parse_hand(line_ind)
            self.set_ranks(hand)

        grand_total = 0
        for rank, hand in self.ranks.items():
            grand_total += hand["hand_wager"] * rank
        return grand_total
    
    # Parses the hand string to return a hand dictionary. I made the choice to save each
    # card in memory so that set_rank would be a bit more readable, instead of just 
    # using subscripts to indicate which character to read.
    def parse_hand(self, line_ind: int) -> dict: 
        hand_cards, hand_wager = self.hands_data[line_ind].split(' ')
        card_set_length = len(set(hand_cards))
        hand_strength = self.strength_test(card_set_length, hand_cards)
        return {
            "hand_cards": hand_cards,
            "first_card": hand_cards[0],
            "second_card": hand_cards[1],
            "third_card": hand_cards[2],
            "fourth_card": hand_cards[3],
            "fifth_card": hand_cards[4],
            "hand_strength": hand_strength, 
            "hand_wager": int(hand_wager)
        }

    # This method sets a hand to the lowest rank that it unfilled or that is filled by
    # a stronger hand. That stronger hand is saved and set_rank is called again to 
    # relocate the old hand. This happens recursively, and it stops happening when the 
    # last card replaces the highest card, and the highest card is moved to rank[1000].
    def set_ranks(self, hand: dict) -> None:
        for rank in self.ranks.keys():
            
            if not self.ranks[rank]:
                self.ranks[rank] = hand
                break
            else:
                rank_hand = self.ranks[rank]
                rank_hand_strgth = rank_hand["hand_strength"]
                rank_hand_cards = rank_hand["hand_cards"]
                if (
                    rank_hand_strgth > hand["hand_strength"] or
                    (
                        rank_hand_strgth == hand["hand_strength"] and
                        self.card_values[
                            rank_hand["first_card"]
                        ] > self.card_values[hand["first_card"]]
                    ) or (
                        rank_hand_strgth == hand["hand_strength"] and
                        rank_hand["first_card"] == hand["first_card"] and
                        self.card_values[
                            rank_hand["second_card"]
                        ] > self.card_values[hand["second_card"]]
                    ) or (
                        rank_hand_strgth == hand["hand_strength"] and
                        rank_hand_cards[0:2] == hand["hand_cards"][0:2] and
                        self.card_values[
                            rank_hand["third_card"]
                        ] > self.card_values[hand["third_card"]]
                    ) or (
                        rank_hand_strgth == hand["hand_strength"] and
                        rank_hand_cards[0:3] == hand["hand_cards"][0:3] and
                        self.card_values[
                            rank_hand["fourth_card"]
                        ] > self.card_values[hand["fourth_card"]]
                    ) or (
                        rank_hand_strgth == hand["hand_strength"] and
                        rank_hand_cards[0:4] == hand["hand_cards"][0:4] and
                        self.card_values[
                            rank_hand["fifth_card"]
                        ] > self.card_values[hand["fifth_card"]]
                    )
                ):
                    self.shift_rank(rank, hand)
                    break
                    
    # This takes a rank and a hand, swaps the old hand for the new, and checks if there
    # is a hand in the next rank before calling itself, or calling set_rank.
    def shift_rank(self, rank:int, hand: dict) -> None:
        old_hand = self.ranks[rank]
        self.ranks[rank] = hand
        if self.ranks.get(rank + 1):
            self.shift_rank(rank + 1, old_hand)
        else:
            self.set_ranks(old_hand) 
                    

if __name__ == "__main__":
    handscorerer = HandScorer('day-7/hands.txt')

    print(handscorerer.hands_calc())
